package HW;

public class SolveLine {

	public static void main(String[] args) {
		///homework 3 from 04.12
		double a1 = 1,a2 = 1,b1 = 1,b2 = -1,c1 = 10,c2 = 6;
		
		double det = (a1*b2) - (a2*b1);
		double mx = (c1*b2) - (c2 * b1);
		double my = (a1 * c2) - (a2*c1);
		double x = (mx/det);
		double y = (my / det);
		
		System.out.println("Solution is: x = " + x + "; y = " + y);

	}

}
