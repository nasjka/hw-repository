package HW;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;



public class ExecutionAsgn1 {
	
	

	public static void main(String[] args) {
		Officer[] officers = new Officer[7];
		
		for (int i = 0; i < 7; i++)
			officers[i] = new Officer();
		
		officers[0].setName("Katrina");
		officers[0].setSurname("Irbe");
		officers[0].setOfficerID(200);
		officers[0].setCrimesSolved(25);
		
		officers[1].setName("Daniel");
		officers[1].setSurname("White");
		officers[1].setOfficerID(333);
		officers[1].setCrimesSolved(70);
		
		officers[2].setName("John");
		officers[2].setSurname("Arvin");
		officers[2].setOfficerID(423);
		officers[2].setCrimesSolved(30);
		
		officers[3].setName("Betty");
		officers[3].setSurname("Silver");
		officers[3].setOfficerID(459);
		officers[3].setCrimesSolved(87);
		
		officers[4].setName("Andy");
		officers[4].setSurname("Silevan");
		officers[4].setOfficerID(332);
		officers[4].setCrimesSolved(19);
		
		officers[5].setName("Tom");
		officers[5].setSurname("Ford");
		officers[5].setOfficerID(876);
		officers[5].setCrimesSolved(88);
		
		officers[6].setName("Harald");
		officers[6].setSurname("Joly");
		officers[6].setOfficerID(473);
		officers[6].setCrimesSolved(54);
		
		
		District district1 = new District("District1", "New-York", 1);
		District district2 = new District("District2", "San Francisko", 2);

		for (int i = 0; i < 3; i++)
			district1.addNewOfficer(officers[i]);

		for (int i = 3; i < 7; i++)
			district2.addNewOfficer(officers[i]);
		
		Lawyer lawyer1 = new Lawyer("Dana", "Lorden", 222, 3);
		Lawyer lawyer2 = new Lawyer("Andry", "Star", 77, 8);
		Lawyer lawyer3 = new Lawyer("Katrin", "White", 10, 5);
		
		System.out.println(district1);
		System.out.print(System.lineSeparator());
		System.out.println(district2);
		System.out.print(System.lineSeparator());
		
		System.out.println(
				"There are " + district1.getOfficersInTheDistrict().size() + " officers in the first district");
		System.out.println(
				"There are " + district2.getOfficersInTheDistrict().size() + " officers in the second district");
		
		System.out.print(System.lineSeparator());
		
		System.out.println(lawyer1);
		System.out.print(System.lineSeparator());
		System.out.println(lawyer2);
		System.out.print(System.lineSeparator());
		System.out.println(lawyer3);
		System.out.print(System.lineSeparator());
		
		ArrayList<Lawyer> arrList = new ArrayList<Lawyer>();
		arrList.add(lawyer1);
		arrList.add(lawyer2);
		arrList.add(lawyer3);
		


		Iterator<Lawyer> iteratorLawyers = arrList.iterator();
		int crimes = 0;
		Lawyer bestresult = null;
		
		while (iteratorLawyers.hasNext()) {
			Lawyer next = iteratorLawyers.next();//// get the person from the collection
			crimes += next.getHelpedinCrimesSolvin();

			if (bestresult == null || bestresult.getHelpedinCrimesSolvin() < next.getHelpedinCrimesSolvin())
					bestresult = next;
				
		}

			System.out.print(System.lineSeparator());
		    System.out.println("Total crimes in which Lawyers were involved: " + crimes);
		    
		    System.out.print(System.lineSeparator());
		    System.out.println("Lawyer who helped the most is : " + System.lineSeparator() + bestresult);
		    
				
			
		    
		    
		}

	
}
