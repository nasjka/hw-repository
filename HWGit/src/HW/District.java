package HW;

import java.util.ArrayList;

public class District {
	private String title, city;
	private int districtID;
	private ArrayList<Officer> officersInTheDistrict = new ArrayList<Officer>();
	
	public District() {
		
	}
	
	public District(String title, String city,int districtID) {
		this.title = title;
		this.city = city;
		this.districtID = districtID;
	}
	
	public String getCity() {
		return city;
	}
	public int getDistrictID() {
		return districtID;
	}
	public ArrayList<Officer> getOfficersInTheDistrict() {
		return officersInTheDistrict;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setCity(String city) {
		this.city = city;
	}
	
	public void setDistrictID(int districtID) {
		this.districtID = districtID;
	}
	
	public void setOfficersInTheDistrict(ArrayList<Officer> officersInTheDistrict) {
		this.officersInTheDistrict = officersInTheDistrict;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	

	@Override
	public String toString() {
		return "Title: " + this.title + System.lineSeparator() + "City: " + this.city + System.lineSeparator() + "District ID: "  + this.districtID;
	}
	
	public void addNewOfficer (Officer officer) {
		this.officersInTheDistrict.add(officer);
	}
	
	public void removeOfficer (Officer officer) {
		this.officersInTheDistrict.remove(officer);
	}
	
	public float calculateAvgLevelInDistrict() {
		float levelSum = 0;
		
		for (int i = 0; i < this.officersInTheDistrict.size(); i++)
			levelSum += this.officersInTheDistrict.get(i).calculateLevel();

		return levelSum / this.officersInTheDistrict.size();
		
	}

}
