package HW;

import java.util.ArrayList;
import java.util.Iterator;

public class District2 {
	private String title, city;
	private int districtID;
	private ArrayList<Person2> personInTheDistrict = new ArrayList<Person2>();
	
	public District2() {
		
	}
	
	public District2(String title, String city,int districtID) {
		this.title = title;
		this.city = city;
		this.districtID = districtID;
	}
	
	public String getCity() {
		return city;
	}
	public int getDistrictID() {
		return districtID;
	}
	public ArrayList<Person2> getpersonInTheDistrict() {
		return personInTheDistrict;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setCity(String city) {
		this.city = city;
	}
	
	public void setDistrictID(int districtID) {
		this.districtID = districtID;
	}
	
	public void setpersonInTheDistrict(ArrayList<Person2> personInTheDistrict) {
		this.personInTheDistrict = personInTheDistrict;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	
	

	@Override
	public String toString() {
		return "Title: " + this.title + System.lineSeparator() + "City: " + this.city + System.lineSeparator() + "District ID: "  + this.districtID;
	}
	
	public void addNewPerson (Person2 person) {
		this.personInTheDistrict.add(person);
	}
	
	public void removePerson (Person2 person) {
		this.personInTheDistrict.remove(person);
	}
	
	public float calculateAvgLevelInDistrict() {
		
		int levelTotal=0, numberOfOfficers=0;
		
		Iterator <Person2> iterator = this.personInTheDistrict.iterator();		
		while (iterator.hasNext()) {
			Person2 persons =  iterator.next() ;
				if (persons instanceof Officer2) {
					Officer2 officer = (Officer2) persons;
					numberOfOfficers++;
					levelTotal += officer.getCrimesSolved();
					
				}
				
			}
		
		return (float) levelTotal / (float)numberOfOfficers;
		}

		
	}


