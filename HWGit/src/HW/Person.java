package HW;


public class Person {
	
	protected String name, surname;
	
	public String getName() {
		return name;
	}
	
	public String getSurname() {
		return surname;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setSurname(String surname) {
		this.surname = surname;
	}
	
	public Person () {
		
	}
	
	public Person(String name, String surname) {
		this.name = name;
		this.surname = surname;
		
	
	}
	
	
	public static void main(String[] args) {

		
		Person person1 = new Person ("Linda","Brown");
		System.out.println(person1);
	}
			
	
	public String getContact() {
		return "Name: " + this.name + System.lineSeparator() + "Surname: " + this.surname;
	}
	
	@Override
	public String toString() {
		return getContact();
	} 



}