package HW;

public class Officer2 extends Person2 {
	private int officerID, crimesSolved;

	public int getCrimesSolved() {
		return crimesSolved;
	}
	
	public int getOfficerID() {
		return officerID;
	}
	public void setCrimesSolved(int crimesSolved) {
		this.crimesSolved = crimesSolved;
	}
	public void setOfficerID(int officerID) {
		this.officerID = officerID;
	}
	
	public Officer2 () {
		
	}
	
	public Officer2 (String name, String surname, int officerID, int crimesSolved) {
		super (name,surname);
		this.officerID = officerID;
		this.crimesSolved = crimesSolved;
		
		
	}
	
	
	public static void main(String[] args) {

		
		Officer officer1 = new Officer ("David","Backham", 23, 55);
		System.out.println(officer1);
			

	}
	
	@Override
	public String toString() {
		return  super.toString() + System.lineSeparator() + "Officer ID: " + this.officerID + System.lineSeparator() + "Solved crimes: " + this.crimesSolved;
	}
		
	
	int calculateLevel() {
		if (this.crimesSolved < 20)
			return 1;
		else if (this.crimesSolved >= 20 && this.crimesSolved < 40)
			return 2;
		else
			return 3;

	}

}
