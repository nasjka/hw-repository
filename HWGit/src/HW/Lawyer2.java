package HW;

public class Lawyer2 extends Person2 {
	
private int lawyerID, helpedinCrimesSolving;
	
	public void setLawyerID(int lawyerID) {
		this.lawyerID = lawyerID;
	}

	public void setHelpedinCrimesSolving(int helpedinCrimesSolving) {
		this.helpedinCrimesSolving = helpedinCrimesSolving;
	}

	public int getLawyerIDt() {
		return lawyerID;
	}

	public int getHelpedinCrimesSolvin() {
		return helpedinCrimesSolving;
	}
	
	
	public Lawyer2() {
		
	}
	
	public Lawyer2(String name, String surname, int lawyerID, int helpedinCrimesSolving) {
		
		super(name, surname);
		this.lawyerID = lawyerID;
		this.helpedinCrimesSolving = helpedinCrimesSolving;
		
		
	}
	

	
	
	
	public static void main(String[] args) {

	
		Lawyer lawyer1 = new Lawyer ("John","Black", 245, 100);
		System.out.println(lawyer1);

			

	}
	
	@Override
	public String toString() {
		return  super.toString() + System.lineSeparator() + "Lawyer ID: " + this.lawyerID + System.lineSeparator() + "Helped in Crimes solving:" + this.helpedinCrimesSolving;
	}
		


}
