package tic_tactoe;

import java.util.Random;
import java.util.Scanner;

public class TicTacToe {
	
	final char Sing_X = 'x';
	final char Sing_O = 'o';
	final char Sign_empty = '*';
	static char[][] table;
	Random random;
	Scanner scanner;

	public static void main(String[] args) {
		new TicTacToe().game();

	}
	
	TicTacToe() {
		random = new Random();
		scanner = new Scanner(System.in);
		table = new char [3][3];
		
	}
	
	void game() {
	
		initTable();
		printTable();

		while (true) {
			humanTurn();
			if (checkWin(Sing_X)) {
				System.out.println("Player Win!");
				break;
			}
			
			if (isTableFull()) {
				System.out.println("Draw!");
				break;
			}
			
			compTurn();
			printTable();
			if (checkWin(Sing_O)) {
				System.out.println("Computer Win!");
				break;
		}
			
			if (isTableFull()) {
				System.out.println("Draw!");
				break;
			}
	}
	System.out.println("Game Over");
	printTable();

	}
	
	void initTable() {
		for (int row = 0; row < 3; row++)
			for (int col = 0; col < 3; col++)
				table[row][col] = Sign_empty;
	}
	
	public static void printTable() {
		System.out.println("0 1 2 3 ");
		for (int row = 0; row < 3; row++) {
			System.out.print((row + 1) + " ");
			for (int col = 0; col < 3; col++)
				System.out.print(table[row][col] 
					+ " ");
			System.out.println();
		}
	}
	

	
	void humanTurn() {
		int x, y;
		do {
			System.out.println("Please enter X and Y (from 1 - 3):");
			
			x = scanner.nextInt() - 1;
			y = scanner.nextInt() - 1;
			
		} while (!isCellValid(x,y));
		table[y][x] = Sing_X;
	}
	
	boolean isCellValid(int x, int y) {
		if (x < 0 || y < 0 || x >=3 || y >= 3)
			return false;
		return table[y][x] == Sign_empty;
	}
	
	void compTurn() {
		int x,y;
		do {
			x = random.nextInt(3);
			y = random.nextInt(3);
		} while (!isCellValid(x, y));
		table [y][x] = Sing_O;
	}
	
	

	
	boolean checkWin(char ox) {
		for (int i = 0; i < 3; i++)
			if ((table [i][0] == ox && table [i][1] == ox && table [i][2] == ox)
					|| (table [0][i] == ox && table [1][i] == ox && table [2][i] == ox))
				return true;
			if ((table [0][0] == ox && table [1][1] == ox && table[2][2] == ox)
					|| (table [2][0] == ox && table [1][1] == ox && table [0][2] == ox))
				return true;
			return false;
	}
	
	boolean isTableFull() {
		for (int row = 0; row < 3; row++)
			for (int col = 0; col < 3; col++)
				if (table [row][col] == Sign_empty)
					return false;
		return true;
					
	}
}
